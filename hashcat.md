`sudo apt install hashcat`

# Download a word list
GOOGLE -> "download rockyou.txt"
```bash
sudo mkdir /usr/share/wordlists
sudo mv rockyou.txt /usr/share/wordlists
```
*(**/usr/share/wordlists** is the default location of **rockyou.txt** on Kali Linux)*

# Create a file with hash to hack
```bash
echo -n "pa55word" | md5sum | tr -d " -" >> test
echo -n "123456" | md5sum | tr -d " -" >> test
cat test
87a45243787b42eda54f0e2a37881c84
e10adc3949ba59abbe56e057f20f883e
```

`-n` : new line
`md5sum` : convert to md5sum hash
`tr -d " -"` : remove all **'spaces'** and **'-'** characters from the result

# Run hashcat
`hashcat -m 0 -a 0 -o result test /usr/share/wordlists/rockyou.txt`

`-m 0` : hash type (0 = md5sum)
`-a 0` : attack mode
`-o result` : output file
*(**hashcat --help** for more info)*

```bash
cat result
e10adc3949ba59abbe56e057f20f883e:123456
a17a41337551d6542fd005e18b43afd4:pa55word
```
